using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools.Commands
{
    public class UppercaseSelectionCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;

        public UppercaseSelectionCommand(IExcelAccessor excelAccessor) 
        : base("Uppertcase selected cells", new [] {KnownContextMenuLocations.Cell, KnownContextMenuLocations.Table}, "Text", 2, 100)
        {
            this.excelAccessor = excelAccessor;
        }

        public override void Execute()
        {
            excelAccessor.Application.GetSelectedRange().UpdateValues(v => v?.ToString().ToUpper());
        }
    }
}
