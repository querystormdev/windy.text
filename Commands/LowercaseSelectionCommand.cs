using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools.Commands
{
    public class LowercaseSelectionCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;

        public LowercaseSelectionCommand (IExcelAccessor excelAccessor) 
        	: base(
        		caption: "Lowercase selected cells", 
        		allowedLocations: new [] {KnownContextMenuLocations.Cell, KnownContextMenuLocations.Table}, 
        		category: "Text", 
        		order: 1, 
        		faceId: 91)
        {
            this.excelAccessor = excelAccessor;
        }

        public override void Execute()
        {
            excelAccessor.Application.GetSelectedRange().UpdateValues(v => v?.ToString().ToLower());
        }
    }
}
