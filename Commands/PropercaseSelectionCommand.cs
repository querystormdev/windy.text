using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools.Commands
{
    public class PropercaseSelectionCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;

        public PropercaseSelectionCommand(IExcelAccessor excelAccessor) 
        : base("Propercase selected cells", new [] {KnownContextMenuLocations.Cell, KnownContextMenuLocations.Table}, "Text", 3, 95)
        {
            this.excelAccessor = excelAccessor;
        }

        public override void Execute()
        {
            excelAccessor.Application.GetSelectedRange().UpdateValues(v => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(v?.ToString().ToLower() ?? ""));
        }
    }
}
