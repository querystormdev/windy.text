using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Text
{
	public class StringFunctions
	{
		[ExcelFunction(Name = "Windy.SubString")]
        public static string SubString(string input, int position)
            => input.Substring(position);

        [ExcelFunction(Name = "Windy.IndexOf")]
        public static int IndexOf(string input, string subString)
            => input.IndexOf(subString);
            
        [ExcelFunction(Name = "Windy.ReplaceAt")]
        public static string ReplaceAt(string input, int index, int length, string replacementString)
            => input.ReplaceAt(index, length, replacementString);

        [ExcelFunction(Name = "Windy.Format")]
        public static string Format(string inputTemplate, object arg1, object arg2, object arg3, object arg4, object arg5, object arg6, object arg7, object arg8, object arg9, object arg10)
            => string.Format(inputTemplate, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
            
        [ExcelFunction(Name = "Windy.FormatWithCulture")]
        public static string FormatWithCulture(string culture, string inputTemplate, object arg1, object arg2, object arg3, object arg4, object arg5, object arg6, object arg7, object arg8, object arg9, object arg10)
            => string.Format(CultureInfo.GetCultureInfo(culture), inputTemplate, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
	}
}
