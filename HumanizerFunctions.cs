using Humanizer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy_Text
{
    public class HumanizerFunctions
    {
        [ExcelFunction(Name = "Windy.ToWords", Description = "Converts a number into its word representation, according to the given culture (e.g. 1 => one)")]
        public static string ToWords(int number, string culture = null)
        {
            var cultureInfo = string.IsNullOrWhiteSpace(culture)
                ? CultureInfo.CurrentCulture
                : CultureInfo.GetCultureInfo(culture);

            var result = number.ToWords(cultureInfo);

            return result;
        }

        [ExcelFunction(Name = "Windy.ToOrdinalWords", Description = "Converts a number into its ordinal word representation, according to the given culture (e.g. 1 => first)")]
        public static string ToOrdinalWords(int number, string culture = null)
        {
            var cultureInfo = string.IsNullOrWhiteSpace(culture)
                ? CultureInfo.CurrentCulture
                : CultureInfo.GetCultureInfo(culture);

            return number.ToOrdinalWords(cultureInfo);
        }
    }
}
