using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Text
{
	public class CurrencyFunctions
	{
		[ExcelFunction(Name = "Windy.ToCurrencyString", Description = "Format a number as money according to the given culture identifier (e.g. en-US, de-DE...)")]
		public static string ToCurrencyString(double amount, string cultureName)
		{
			var culture = CultureInfo.GetCultureInfo(cultureName);
			return amount.ToString("C", culture);
		}
	}
}
