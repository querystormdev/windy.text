using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Text
{
	public class RegexFunctions
	{
		[ExcelFunction(Name = "Windy.IsMatch", Description = "Indicates whether the specified regular expression finds a match in the given input string.")]
		public static bool IsMatch(string input, string pattern)
		{
			return Regex.IsMatch(input, pattern);
		}
		
		[ExcelFunction(Name = "Windy.RegexReplace", Description = "In an input string, replaces all substrings that match the given pattern with the specified replacement string.")]
		public static string RegexReplace(string input, string pattern, string replacement)
		{
			return Regex.Replace(input, pattern, replacement);
		}
		
		static readonly Regex _templateNumberedGroupRegex = new Regex(@"^((?'group'(\$\d+)|(\$\{\w+\}))|.)*$");
		[ExcelFunction(Name = "Windy.RegexTransform", Description = "Returns a string created based on the specified template which is populated by matches from the input string.")]
        public static string RegexTransform(string input, string pattern, string outputTemplate = "$0")
        {
            var m = Regex.Match(input, pattern, RegexOptions.Singleline);
            
            var templateMatch = _templateNumberedGroupRegex.Match(outputTemplate);

            string output = outputTemplate;
            foreach (var tm in templateMatch.Groups["group"].Captures.OfType<Capture>().OrderByDescending(c => c.Index))
            {
                string groupName = tm.Value.TrimStart('$').TrimStart('{').TrimEnd('}');

                var groupValue = m.Groups[groupName].Value;

                output = output.ReplaceAt(tm.Index, tm.Length, groupValue);
            }

            return output;
        }
	}
}
