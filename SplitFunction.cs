using System;
using QueryStorm.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using QueryStorm.Apps;
using static QueryStorm.Tools.DebugHelpers;
using System.Text.RegularExpressions;

namespace Windy_Text
{
	public class SplitFunction
	{
		[ExcelFunction(Name = "Windy.Split", Description = "Divides text around a specified character or string, and puts each fragment into a separate cell in the row.")]
		public static string[,] Split(string input, string delimiters, bool splitOnEach = true, bool vertical = false)
		{
			string [] items;
			var options = StringSplitOptions.RemoveEmptyEntries;
			if(splitOnEach)
				items = input.Split(delimiters.ToArray(), options);
			else
				items = input.Split(new string [] { delimiters }, options);
				
			return vertical ? items.To2DArrayVertical() : items.To2DArrayHorizontal();
		}
		
		[ExcelFunction(Name = "Windy.RegexSplit", Description = "Splits an input string into an array of substrings at the positions defined by a regular expression pattern.")]
		public static string[,] RegexSplit(string input, string pattern, bool vertical = false)
		{
			var items = Regex.Split(input, pattern);
			return vertical ? items.To2DArrayVertical() : items.To2DArrayHorizontal();
		}
	}
}
